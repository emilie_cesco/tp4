
//variable game over
let gameovertext = document.getElementById("gameovertext");
let boxGameover = document.getElementById("gameover");
let descente = -5;


//variable bar manger
let manger = 100;
let progManger = document.getElementById("progManger");
let buttonManger = document.getElementById("Btnmanger");

//variable ma console

let maConsole = document.getElementById('console');
let fenetreConsole = document.getElementById('fenetre-console');
let retourConsole = document.getElementById('retour-console')
let temp = 0;
let onglet = document.getElementById('onglet')
let ongletOnOff = false;
let memoireConsole;


//animation console
onglet.addEventListener('click', animate)

function animate() {
    if (ongletOnOff == false) {

        ongletOnOff = true
        fenetreConsole.style.animationName = "deroulement"
        fenetreConsole.style.animationDuration = "2s"
        fenetreConsole.style.animationFillMode = "forwards"
        fenetreConsole.style.animationIterationCount = "1"
    }
    else {
        ongletOnOff = false
        fenetreConsole.style.animationName = "enfilage"
        fenetreConsole.style.animationDuration = "2s"
        fenetreConsole.style.animationFillMode = "backwards"
        fenetreConsole.style.animationIterationCount = "1"
    }
}

//recharger bar manger

buttonManger.addEventListener("click", jemange)

function jemange() {
    if (manger <= 90);
    manger = manger + 10;
    progManger.value = manger;
}

//recharger bar boire

let boire = 100;
let progBoire = document.getElementById("progBoire");
let buttonBoire = document.getElementById("Btnboire")
buttonBoire.addEventListener("click", jebois)

function jebois() {
    if (boire <= 90);
    boire = boire + 10;
    progBoire.value = boire;
}

//recharger bar domir

let dormir = 100;
let progDormir = document.getElementById("progDormir");
let buttonDormir = document.getElementById("Btndormir")
buttonDormir.addEventListener("click", jedors)
function jedors() {
    if (dormir <= 90);
    dormir = dormir + 10;
    progDormir.value = dormir;
}

//fonction pour gerér la descente des bars

function descenteManger() {
    boire = boire + descente;
    progBoire.value = boire;
    manger = manger + descente;
    progManger.value = manger;
    dormir = dormir + descente;
    progDormir.value = dormir;
    if (progManger.value <= 0 || progBoire.value <= 0 || progDormir.value <= 0) {
        boxGameover.style.visibility = "visible";
        gameovertext.style.animationDuration = "2s"
        gameovertext.style.animationFillMode = "forwards"
        gameovertext.style.animationName = "animegameover";

    }
}
setInterval(descenteManger, 1000)

let buttonRefresh = document.getElementById("btnRefresh")

buttonRefresh.addEventListener("click", function () {
    document.location.reload(true);
})


//

window.addEventListener('keydown', event => {
    if (event.keyCode === 13) {
        temp++

        //  memoireConsole=maConsole.value;
        if (maConsole.value == "boire") { jebois() }
        if (maConsole.value == "manger") { jemange() }
        if (maConsole.value == "dormir") { jedors() }
        if (maConsole.value == "help") { }
    
        let p = document.createElement('p')

        p.style.bottom = "0px"
        p.setAttribute('id', "p" + temp)
        p.innerText = maConsole.value;
        console.log(temp)
        retourConsole.prepend(p);
    }
})


window.addEventListener('keyup', event => {
    if (event.keyCode === 13) {
        maConsole.value = ''
    }
})

